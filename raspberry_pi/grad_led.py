#coding: utf-8

import wiringpi as pi
import time

LED_PIN = 23
pi.wiringPiSetupGpio()
pi.pinMode(LED_PIN, pi.OUTPUT)
pi.softPwmCreate(LED_PIN, 0, 100)               # PWMを使用可能に

while True:
    brightness = 0                              # 現在のLEDの明るさを保持する変数
    while (brightness < 100):                   # 徐々に明るくする
        pi.softPwmWrite(LED_PIN, brightness)
        time.sleep(0.1)
        brightness = brightness + 1
    while (brightness > 0):                     # 徐々に暗くする
        pi.softPwmWrite(LED_PIN, brightness)
        time.sleep(0.1)
        brightness = brightness - 1