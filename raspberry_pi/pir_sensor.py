#coding: utf-8

import wiringpi as pi
import time

PIR_PIN = 18
pi.wiringPiSetupGpio()
pi.pinMode(PIR_PIN, pi.INPUT) # PIR_PINをINPUTにする

while True:
    if (pi.digitalRead(PIR_PIN) == pi.HIGH):
        print("A person was detected.")
        time.sleep(15) # 検知後はやや長めに待つ
    else:
        time.sleep(3) 