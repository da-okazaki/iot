#coding: utf-8

import sys
import time
import mcp3008 # MCP3008用モジュールを取り込む

CH = 2 # CdSはチャネル2につなぐ

try:
    while True:
        data = mcp3008.readAdcValue(CH) # raw値を読む
        print("adc: {:4} ".format(data))
        time.sleep(0.2)

except KeyboardInterrupt: # キーボード割り込みの時
sys.exit(0) # プログラムを終了する