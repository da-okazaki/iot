#coding: utf-8

import wiringpi as pi
import time
import i2c_lcd # I2C LCD用モジュールを取り込む

pi.wiringPiSetupGpio()

i2c_lcd.lcd_init() # i2c_lcdを初期化する
i2c_lcd.lcd_string(“Hello, world!”, i2c_lcd.LCD_LINE_1)
i2c_lcd.lcd_string(“ with Ras-pi”, i2c_lcd.LCD_LINE_2)
time.sleep(30) # しばらくLCDに表示しておく
i2c_lcd.lcd_clear() # LCDをクリアする