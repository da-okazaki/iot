#coding: utf-8

import wiringpi as pi
import time
BUZZER_PIN = 23
pi.wiringPiSetupGpio()
pi.pinMode(BUZZER_PIN, pi.OUTPUT)

while True:
    pi.digitalWrite(BUZZER_PIN, pi.HIGH) # ブザーを鳴らす
    time.sleep(0.5)
    pi.digitalWrite(BUZZER_PIN, pi.LOW) # ブザーを止める
    time.sleep(0.5)