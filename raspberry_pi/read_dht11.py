#coding: utf-8

import wiringpi as pi
import time
import dht11 # DHT11用モジュールを取り込む

pi.wiringPiSetupGpio()
sensor = dht11.DHT11(pin=4)

while True:
    result = sensor.read() # DHT11を読み取る
    if result.is_valid(): # 正しく読み取れたら処理する
        print("Temperature: %d C" % result.temperature)
        print("Humidity: %d %%" % result.humidity)
    time.sleep(1) # 1秒おきに繰り返す