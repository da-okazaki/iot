#coding: utf-8

import sys
import time
import mcp3008 # MCP3008用モジュールを取り込む

CH = 0 # ボリュームはチャネル0につなぐ

try:
    while True:
        data = mcp3008.readAdcValue(CH)
        print("adc: {:4} ".format(data))
        mV = mcp3008. convertVoltage(data)
        print("mV: {:4}".format(mV))
        time.sleep(0.2)

except KeyboardInterrupt: # キーボード割り込み時
sys.exit(0) # プログラムを終了する