#coding: utf-8

import wiringpi as pi
import time

LED_PIN = 23 # 変数LED_PINを定義
pi.wiringPiSetupGpio() # wiringPiを初期化
pi.pinMode(LED_PIN, pi.OUTPUT) # GPIO23をOUTPUTに

while True:
    pi.digitalWrite(LED_PIN, pi.HIGH) # LEDを点灯
    time.sleep(0.5) # 0.5秒待つ
    pi.digitalWrite(LED_PIN, pi.LOW) # LEDを消灯
    time.sleep(0.5) # 0.5秒待つ