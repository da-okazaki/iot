#coding: utf-8

import wiringpi as pi
import time
import sys

LED_PIN = 23
SW_PIN = 24
BUZZER_PIN = 18
ONE_TIME = 0.5

pi.wiringPiSetupGpio()                
pi.pinMode(LED_PIN, pi.OUTPUT)        
pi.pinMode(SW_PIN, pi.INPUT)          
pi.pullUpDnControl(SW_PIN, pi.PUD_UP) 
pi.pinMode(BUZZER_PIN, pi.OUTPUT)     

try:
  while True:
    count_timer = 0
    if (pi.digitalRead(SW_PIN) == pi.LOW):
      while (count_timer < 180):
        count_timer = count_timer + 1
        print(count_timer)
        time.sleep(1)

      while True:    
        pi.digitalWrite(LED_PIN, pi.HIGH)    #1.LEDを点滅
        pi.digitalWrite(BUZZER_PIN, pi.HIGH) #2.ブザーを鳴らす
        time.sleep(ONE_TIME)                 #3.一定時間待つ
        pi.digitalWrite(LED_PIN, pi.LOW)     #4.LEDを消灯
        pi.digitalWrite(BUZZER_PIN, pi.LOW)  #5.ブザーを止める
        time.sleep(ONE_TIME)                 #6.一定時間待つ

        if (pi.digitalRead(SW_PIN) == pi.LOW):
          time.sleep(ONE_TIME)
          break

except KeyboardInterrupt:
  pi.digitalWrite(LED_PIN, pi.LOW)
  pi.digitalWrite(BUZZER_PIN, pi.LOW)
  sys.exit(0)